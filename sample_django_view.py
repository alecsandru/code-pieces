"""
This is a sample code containing 3 different views used in a project which aims to evaluate employees based on their
designated roles & based on certain 'competencies' each role can have.

It contains queryset processing, session manipulation, data aggregation.

The HTMLs corresponding to this views were built on Bootstrap & JS. No frontend framework used, so all the logic regarding
how the data should look on the context had to be done here.

EvaluationView:
    This is the view which handles the navigation through questions while evaluating the employee.
    It uses session to keep track of what questions should be presented as well as storing the value for each one.
    At the end it saves employee's responses

ReportView:
    This is the view which renders how well an employee scored on his competencies.
    It also calculates where the employee is on a heatmap (based on coordinates from JS file)

GroupReportView:
    Mostly the same thing as the ReportView but for a set of employees.

"""
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.db.models import Avg, Q
from django.utils.datetime_safe import datetime
from django.views import View
from django.views.generic import DetailView, ListView
from functools import reduce

from evaluations.models import Competency, CompetencyResponses, EmployeeEvaluation
from users.models import Employee


class EvaluationView(DetailView):
    template_name = 'evaluations/evaluation.html'
    model = Competency

    def _set_ids_on_session(self, username, competency_ids):
        self.request.session[str(username)] = competency_ids

    def _get_ids_from_session(self, username):
        user_competency_ids = self.request.session.get(str(username))
        return user_competency_ids

    def _set_user_answer(
            self, employee_id, competency_id, answer
    ):
        self.request.session.modified = True
        answers_key = "{0}_answers".format(employee_id)
        answers = self.request.session.setdefault(answers_key, {})
        answers[str(competency_id)] = answer

    def _process_evaluation_data(self, employee_id):
        EmployeeEvaluation.objects.filter(
            employee_id=employee_id,
            latest=True
        ).update(
            latest=False
        )
        employee_evaluation = EmployeeEvaluation.objects.create(
            employee_id=employee_id,
            evaluated_at=datetime.now(),
            evaluated_by_id=self.request.user.manager.id,
            latest=True
        )
        # Retrieve all employee data from session
        answers_key = "{0}_answers".format(employee_id)
        user_answers = self.request.session.get(answers_key)
        for k, v in user_answers.items():
            # This can be optimized
            # k -> is the competency_id
            # v -> is the value the user was evaluated to
            competency_response = CompetencyResponses.objects.create(
                competency_id=k,
                narrative_value=v
            )
            employee_evaluation.competency_responses.add(competency_response)

    def _clear_session_for_employee(self, employee_id):
        answers_key = "{0}_answers".format(employee_id)
        self.request.session.pop(answers_key, '')
        self.request.session.pop(str(employee_id), '')

    def _get_answer_from_session(self, employee_id, competency_id):
        answers_key = "{0}_answers".format(employee_id)
        answers_dict = self.request.session.get(answers_key)
        if answers_dict:
            return answers_dict.get(str(competency_id), 500)
        return 500

    def get_object(self, queryset=None):
        employee_id = self.kwargs.get('employee_id')
        user_competency_ids = self._get_ids_from_session(employee_id)
        self.employee = Employee.objects.get(id=employee_id)
        # TBD if one employee can have multiple roles or not
        employee_role = self.employee.roles.first()
        if not user_competency_ids:
            # Means that this is the first time the employee is evaluated
            # Here we'll grab all the competencies he/she will be evaluated for
            # and set their IDs on session appropriately.
            role_competency_ids = list(
                employee_role.competency.all().order_by('?').values_list('id', flat=True)
            )
            # Append IDs from potentials:
            potentials_ids = list(
                Competency.objects.filter(type_of_competency=1).values_list('id', flat=True)
            )
            competency_ids = list(set().union(role_competency_ids, potentials_ids))
            self._set_ids_on_session(employee_id, competency_ids)
            self.object = Competency.objects.get(id=competency_ids[0])
        else:
            # We're in the middle of evaluation for this user.
            # Decide which one is the next competency to be evaluated for
            competency_id = self.request.GET.get('question')
            competency_ids = self._get_ids_from_session(employee_id)
            if competency_id:
                self.object = Competency.objects.get(
                    id=competency_ids[int(competency_id)]
                )
            else:
                self.object = Competency.objects.get(id=competency_ids[0])
        return self.object

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        employee_id = self.kwargs.get('employee_id')
        question_index = self.request.GET.get('question', 0)
        ids_list = self._get_ids_from_session(employee_id)
        if question_index:
            # Check if there's another competency to be evaluated for or if
            # we're done.
            next = int(question_index) + 1
            if next < len(self._get_ids_from_session(employee_id)):
                context['next'] = int(question_index) + 1
                context['prev'] = int(question_index) - 1
                # Ugly hack to fix the navigation. To be fixed!
                if int(question_index) == 1:
                    context['prev'] = 0
            else:
                context['prev'] = int(question_index) - 1
        else:
            context['next'] = 1
            context['prev'] = None

        context['current_question'] = int(question_index) + 1
        context['total_questions'] = len(ids_list)
        context['user_full_name'] = self.employee.user.get_full_name()
        context['user_roles'] = self.employee.pretty_roles
        context['slider_value'] = self._get_answer_from_session(
            employee_id, self.object.id
        )
        return context

    def post(self, request, *args, **kwargs):
        answer = request.POST.get('answer')
        competency_id = request.POST.get('competency_id')
        employee_id = self.kwargs.get('employee_id')
        next = request.POST.get('next')

        self._set_user_answer(
            employee_id=employee_id,
            answer=answer,
            competency_id=competency_id
        )

        if next:
            redirect_to_url = reverse('evaluation_start', kwargs={'employee_id': employee_id})
            redirect_to_url += '?question={0}'.format(next)
        else:
            # This means that the survey is done
            self._process_evaluation_data(employee_id)
            self._clear_session_for_employee(employee_id)
            redirect_to_url = "{0}?employee_id={1}".format(
                reverse('evaluation_complete'), employee_id
            )
        return JsonResponse(
            {
                'redirect_to': redirect_to_url
            }
        )


class ReportView(DetailView):
    template_name = 'evaluations/report.html'
    model = Employee
    slug_url_kwarg = 'employee_id'
    slug_field = 'id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Grab latest evaluation
        employee_evaluation = EmployeeEvaluation.objects.get_latest_for_employee(
            self.object
        )
        context['evaluation_object'] = employee_evaluation
        context['hm_margin_left'], context['hm_margin_top'] = employee_evaluation.get_heatmap_point_coordinates()
        all_responses = employee_evaluation.get_ordered_responses()
        potential_responses = all_responses.get_potentials()
        performance_responses = all_responses.get_performances()

        # split responses into potential and performance
        context['potential_responses'] = potential_responses
        context['performance_responses'] = performance_responses

        context['report_date'] = datetime.now().strftime("%B %d, %Y").upper()

        return context

    def get(self, request, *args, **kwargs):
        # Make sure there's an evaluation for the current employee so we have how to display the report.
        if not EmployeeEvaluation.objects.filter(employee_id=kwargs.get('employee_id')).exists():
            return redirect(reverse('employee_list'))

        return super().get(request, *args, **kwargs)

class GroupReportView(ListView):
    template_name = 'evaluations/multi_report.html'
    model = Employee
    slug_url_kwarg = 'id'
    slug_field = 'id'

    def __process_responses(self, responses, context):
        processed_data = []
        for rsp in responses:
            processed_data.append(
                {
                    'competency_id': rsp.competency.id,
                    'value': rsp.narrative_value
                }
            )
            if str(rsp.competency.id) not in context.keys():
                context[str(rsp.competency.id)] = rsp.competency

        return processed_data

    def _process_performances_responses(self, evaluation, context):
        ordered_responses = evaluation.competency_responses.filter(
            competency__type_of_competency=2
        )
        return self.__process_responses(ordered_responses, context)

    def _process_potentials_responses(self, evaluation, context):
        ordered_responses = evaluation.competency_responses.filter(
            competency__type_of_competency=1
        )
        return self.__process_responses(ordered_responses, context)

    def _get_employee_evaluations(self):
        return EmployeeEvaluation.objects.filter(
            employee__in=self.object_list
        )

    def get_queryset(self):
        ids_list = self.request.GET.getlist('id')
        return self.model.objects.filter(id__in=ids_list).order_by('id')

    def _process_heatmap_points(self, evaluation, context):
        m_left, m_top = evaluation.get_heatmap_point_coordinates()
        context['heatmap_points'].append({
            'name': evaluation.employee.full_name,
            'e_id': evaluation.employee.id,
            'm_left': "{0}px".format(m_left),
            'm_right': "{0}px".format(m_top)
        })

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        performances_block = []
        potentials_block = []
        context['heatmap'] = dict()
        context['heatmap_points'] = list()

        # Grab all evaluations associated with this set of employees
        evaluations = []
        for employee in self.object_list:
            employee_evaluation = EmployeeEvaluation.objects.get_latest_for_employee(employee)
            if employee_evaluation:
                self._process_heatmap_points(employee_evaluation, context)
                evaluations.append(
                    employee_evaluation
                )

        # Process each evaluation and retrieve information
        for evaluation in evaluations:
            performances_block.append(
                self._process_performances_responses(evaluation, context)
            )
            potentials_block.append(
                self._process_potentials_responses(evaluation, context)
            )

        performances = []
        for s in performances_block:
            for x in s:
                performances.append(x)

        potentials = []
        for s in potentials_block:
            for x in s:
                potentials.append(x)

        # Put all the answer values together, for all employees
        potentials_context = dict()
        for item in potentials:
            if str(item['competency_id']) in potentials_context.keys():
                potentials_context[str(item['competency_id'])].append(item['value'])
            else:
                potentials_context[str(item['competency_id'])] = [item['value']]

        performances_context = dict()
        for item in performances:
            if str(item['competency_id']) in potentials_context.keys():
                performances_context[str(item['competency_id'])].append(item['value'])
            else:
                performances_context[str(item['competency_id'])] = [item['value']]

        # Calculate average for potentials and how many people fall under each category
        def _count_in_interval(sequence, interval_start, interval_end):
            total_count = 0
            for value in sequence:
                if interval_start <= value <= interval_end:
                    total_count += 1
            return total_count

        potentials_final = []
        for competency_id, values in potentials_context.items():
            list_average = round(reduce(lambda x, y: x + y, values) / len(values))
            average = round(100 * float(list_average) / 1000.0)
            potentials_final.append(
                {
                    'competency_id': competency_id,
                    'average': average,
                    'low': _count_in_interval(values, 0, 200),
                    'belowaverage': _count_in_interval(values, 201, 400),
                    'normal': _count_in_interval(values, 401, 600),
                    'good': _count_in_interval(values, 601, 800),
                    'well': _count_in_interval(values, 801, 1000),
                    'competency_obj': context[competency_id],
                    'average_arrow_position': average - 7.5
                }
            )

        # Sort potentials
        sorted_potentials_final = sorted(potentials_final, key=lambda k: k['average'], reverse=True)
        context['potentials_final'] = sorted_potentials_final

        performances_final = []
        for competency_id, values in performances_context.items():
            # Based on the answer's average we decide what type of styling to apply when displaying
            # the performances in te report
            list_average = round(reduce(lambda x, y: x + y, values) / len(values))
            competency_obj = context[competency_id]
            performances_final.append(
                {
                    'average': list_average,
                    'competency_title': competency_obj.name,
                    'performance_level_css': CompetencyResponses._get_performance_level_css(list_average),
                    'performance_level': CompetencyResponses._get_performance_level(list_average),
                    'scale_css_classes': CompetencyResponses._scale_css_classes(list_average),
                    'expected_narration_description': competency_obj.narrative_for_value(list_average)
                }
            )

        sorted_performances_final = sorted(performances_final, key=lambda k: k['average'], reverse=True)
        # Split this into lists of 3 objects
        splitted_performances = [
            sorted_performances_final[i:i + 3] for i in range(0, len(sorted_performances_final), 3)
        ]
        context['performances_final'] = splitted_performances

        heatmap_points = context['heatmap_points']
        context['heatmap_points'] = sorted(heatmap_points, key=lambda k: k['e_id'])
        context['report_date'] = datetime.now().strftime("%B %d, %Y").upper()
        return context

    def get(self, request, *args, **kwargs):
        employees_ids_list = request.GET.getlist('id')
        if len(employees_ids_list) == 1:
            return redirect(
                reverse(
                    'report_view',
                    kwargs={'employee_id': employees_ids_list[0]}
                )
            )

        return super().get(request, *args, **kwargs)
