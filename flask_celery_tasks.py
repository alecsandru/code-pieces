"""
Simple celery config & task example.
I didn't c/ped more of the code due to confidentiality issues, although most tasks look the same and their setup is in
setup_periodic_tasks()

For this particular project celery uses RabbitMQ.

You can find the config files for Celery & CeleryBeat in this repository.

The whole thing was deployed on EC2 instance.

"""


def make_celery(app=None):
    app = app or create_app()
    celery = Celery(app.name)
    return celery

celery_app = make_celery()
celery_app.conf.update(
    CELERY_TIMEZONE = 'Etc/UTC'
)

db_conn = None

@worker_process_init.connect
def init_worker(**kwargs):
    db.engine.dispose()
    print('Initializing database connection for worker.')


@celery_app.task
def take_pipeline_snapshot():
    for r in Role.query.all():
        PipelineSnapshot.take_pipeline_snapshot(r)


@celery_app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    """
    Here we register period tasks, which will be ran through Celery Beat.
    They can be registered as period tasks and scheduled to run in specific
    days of the week at specific hours, or at specific intervals.
    """

    # Daily tasks:
    sender.add_periodic_task(
        crontab(hour=10, minute=10),
        take_pipeline_snapshot,
    )
    sender.add_periodic_task(
        crontab(hour=10, minute=00),
        greenhouse_import,
    )
    sender.add_periodic_task(
        crontab(hour=10, minute=30),
        pipeline_update.s('delta', 1),
    )