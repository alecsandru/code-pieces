"""
This is from a Flask application which required to allow people to connect their outlook emails with the app in order
to send emails in their behalf when they wanted.

It handles Oauth2 flow (requesting CODE, exchanging CODE, saving TOKEN and REFRESH TOKEN, making the API request)

Errors occured during the process of generating token OR making the API request are handled a level above in the flask route.
"""
import urllib
import requests
import json


from app import app


OUTLOOK_BASE_URL = 'https://login.microsoftonline.com'
OUTLOOK_AUTH_URL = '{0}{1}'.format(OUTLOOK_BASE_URL, '/common/oauth2/v2.0/authorize?{0}')
OUTLOOK_TOKEN_URL = '{0}{1}'.format(OUTLOOK_BASE_URL, '/common/oauth2/v2.0/token')
OUTLOOK_SCOPES = [
    'https://outlook.office.com/mail.send',
    'https://outlook.office.com/mail.read',
    'offline_access'
]
OUTLOOK_BASE_API_URL = 'https://outlook.office.com/api/v2.0'


class OutlookClient(object):
    def __init__(self, callback_uri=None):
        self.client_id = app.config.get('OUTLOOK_CLIENT_ID')
        self.client_secret = app.config.get('OUTLOOK_CLIENT_SECRET')
        self.callback_uri = callback_uri

    def get_auth_url(self):
        url_params = {
            'client_id': self.client_id,
            'redirect_uri': self.callback_uri,
            'response_type': 'code',
            'scope': ' '.join(OUTLOOK_SCOPES)
        }
        auth_url = OUTLOOK_AUTH_URL.format(
            urllib.urlencode(url_params)
        )
        return auth_url

    def _exchange_code(self, code):
        data = {
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'code': code,
            'redirect_uri': self.callback_uri,
            'grant_type': 'authorization_code'
        }

        try:
            response = requests.post(
                OUTLOOK_TOKEN_URL,
                data=data
            )
        except:
            return False, False

        json_response = response.json()
        return json_response.get('access_token'), json_response.get('refresh_token')

    def get_me(self, token, refresh_token):
        endpoint = '/me'
        response = self._make_authenticated_request(
            endpoint,
            token,
            refresh_token
        )
        if response:
            return response.json()

        return response

    def send_mail(
        self, token, refresh_token, email_subject, email_body, to
    ):
        email_endpoint = '/me/sendmail'
        email_payload = {
            "Message": {
                "Subject": email_subject,
                "Body": {
                    "ContentType": "HTML",
                    "Content": email_body
                },
                "ToRecipients": [
                    {
                        "EmailAddress": {
                            "Address": to
                        }
                    }
                ]
            }
        }

        return self._make_authenticated_post_request(
            email_endpoint, email_payload, token, refresh_token
        )

    def _make_authenticated_request(self, endpoint, token, refresh_token):
        request_url = OUTLOOK_BASE_API_URL + endpoint
        request_headers = {
            'Authorization': 'Bearer {0}'.format(token)
        }
        api_response = requests.get(
            request_url,
            headers=request_headers
        )
        if not api_response.ok:
            refreshed_token = self._refresh_token(refresh_token)
            request_headers = {
                'Authorization': 'Bearer {0}'.format(refreshed_token)
            }
            api_response = requests.get(
                request_url,
                headers=request_headers
            )
            if not api_response.ok:
                return False

        return api_response

    def _make_authenticated_post_request(self, endpoint, body, token, refresh_token):
        request_url = OUTLOOK_BASE_API_URL + endpoint
        request_headers = {
            'Authorization': "Bearer {0}".format(token)
        }
        api_response = requests.post(
            request_url,
            json.dumps(body),
            headers=request_headers
        )
        if not api_response.ok:
            refreshed_token = self._refresh_token(refresh_token)
            request_headers = {
                'Authorization': 'Bearer {0}'.format(refreshed_token),
                'Content-Type': 'application/json'
            }
            api_response = requests.post(
                request_url,
                json.dumps(body),
                headers=request_headers
            )
            if not api_response.ok:
                return False

        return api_response

    def _refresh_token(self, refresh_token):
        data = {
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'refresh_token': refresh_token,
            'redirect_uri': self.callback_uri,
            'grant_type': 'refresh_token'
        }

        try:
            response = requests.post(
                OUTLOOK_TOKEN_URL,
                data=data
            )
        except:
            return False, False

        json_response = response.json()
        return json_response.get('access_token')
