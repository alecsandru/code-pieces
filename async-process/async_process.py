"""
This are some of the views from the project which was designated to
import orders from an .xlsx file and after processing send them the
EasyPost API.

I used a lot request.session during the implementation since the whole
purpose was to build a POC application based on which to extrapolate into
a much more clean product.
"""


from concurrent.futures.thread import ThreadPoolExecutor

from django.contrib import messages
from django.core.exceptions import ImproperlyConfigured
from django.core.urlresolvers import reverse_lazy, reverse
from django.db.models import Q
from django.http.response import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.utils.encoding import force_text
from django.views import generic

from core.easypost_integration import create_easypost_shipment, create_labels
from core.models import OrderFileSettings, Customer, ShippingLabels
from digester.core import process_order_file
from orders.forms import ImportOrderForm, CreateOrderForm, UpdateOrderForm
from orders.models import OrderFile, Order, Shipment


def async_orders_to_easypost(info_dicts):
    with ThreadPoolExecutor(max_workers=20) as executor:
        result = executor.map(create_easypost_shipment, info_dicts)
    return result


class OrdersImportSuccess(generic.View):
    template_name = "orders/import_success.html"

    def get(self, request, *args, **kwargs):
        orders_ids = request.session.get('orders_ids', '')
        customer_id = request.session.get('customer_id', '')
        orders = Order.objects.filter(id__in=orders_ids)
        context = {
            'orders': orders
        }
        # Change this, please alex.
        customer_settings = Customer.objects.get(id=customer_id)
        info_dicts = []
        for order in orders:
            info_data = {
                'order': order,
                'customer_settings': customer_settings
            }
            info_dicts.append(info_data)

        # for i_d in info_dicts:
        #     create_easypost_shipment(i_d)
        async_orders_to_easypost(info_dicts)
        context['eid'] = orders[0].event_id
        return render_to_response(self.template_name, context, context_instance=RequestContext(request))


class CreateLabelsView(generic.View):
    template_name = "orders/create_labels.html"
    success_url = reverse_lazy('orders:batches-success')

    def get_success_url(self):
        """
        Returns the supplied success URL.
        """
        if self.success_url:
            # Forcing possible reverse_lazy evaluation
            url = force_text(self.success_url)
        else:
            raise ImproperlyConfigured(
                "No URL to redirect to. Provide a success_url.")
        return url

    def get(self, request, *args, **kwargs):
        eid = request.GET.get('eid', '')
        if eid:
            orders = Order.objects.filter(event_id=eid)
            c_name = orders[0].customer
            customer_settings = Customer.objects.get(name=c_name)
            # Get only purchased shipments
            shipments = [order.shipment_set.filter(purchased=True).first() for order in orders]
        else:
            orders_ids = request.session.get('orders_ids', '')
            customer_id = request.session.get('customer_id', '')

            orders = Order.objects.filter(id__in=orders_ids)
            customer_settings = Customer.objects.get(id=customer_id)
            shipments = [order.shipment_set.filter(purchased=True).first() for order in orders]

        create_labels(customer_settings, shipments)
        return HttpResponseRedirect(self.get_success_url())


class ImportOrderView(generic.FormView):
    template_name = "orders/import_order.html"
    form_class = ImportOrderForm
    success_url = reverse_lazy('orders:success')

    def form_valid(self, form):
        order_object = form.save(commit=True)
        messages.success(self.request, "Order file uploaded!")
        #TODO: Filter only active settings instance.
        file_settings = OrderFileSettings.objects.filter(customer=order_object.customer).first()
        passed, failed, orders = process_order_file(order_object, file_settings)
        self.request.session['orders_ids'] = [o.id for o in orders]
        self.request.session['customer_id'] = order_object.customer.id
        return HttpResponseRedirect(self.get_success_url())


class OrdersListView(generic.View):
    template_name = "orders/order_files_list.html"

    def get(self, request, *args, **kwargs):
        context = {}
        # Only last 20 orders. Most recent ones.
        context['orders_files'] = OrderFile.objects.all().order_by('-created_at')[:20]
        return render_to_response(self.template_name, context, context_instance=RequestContext(request))


class ShipmentsListView(generic.View):
    filtered_shipments_template_name = "orders/shipments_list.html"
    eids_tempalte_name = "orders/eids_list.html"

    def get(self, request, *args, **kwargs):
        filter_query = request.GET.get('eid')
        context = {}
        if filter_query:
            event_shipments = Shipment.objects.filter(order__event_id=filter_query)
            context['shipments'] = event_shipments
            context['nr_of_shipments'] = len(event_shipments)
            context['purchased_shipments'] = len(event_shipments.filter(purchased=True))
            context['all_shipments_bought'] = len(event_shipments.filter(purchased=False)) == 0
            context['eid'] = event_shipments[0].order.event_id
            template_name = self.filtered_shipments_template_name
        else:
            context['eids'] = set(Order.objects.all().values_list('event_id', flat=True))
            template_name = self.eids_tempalte_name
        return render_to_response(template_name, context, context_instance=RequestContext(request))


class BatchesListView(generic.View):
    template_name = "orders/batch_list.html"

    def get(self, request, *args, **kwargs):
        labels = ShippingLabels.objects.all()
        context = {
            'labels': labels
        }

        return render_to_response(self.template_name, context, context_instance=RequestContext(request))


class EasyPostWebHook(generic.View):
    """
    WebHook for EasyPost API.

    TODO:<
    1. Generic url pattern
    2. Display it somewhere in website so it can be c/p on easypost dashboard for customers
    3. Check post format
    """
    def post(self, request, *args, **kwargs):
        import json
        content_json = json.loads(request.body)
        if content_json.get('result', '').get('object') == 'Batch':
            if content_json.get('result', '').get('state') == 'label_generated':
                response_batch_id = content_json.get('result').get('id')
                response_label_url = content_json.get('result').get('label_url')
                ShippingLabels.objects.filter(batch_id=response_batch_id).update(
                    label_url=response_label_url,
                    status=content_json.get('result', '').get('state')
                )
                return HttpResponse(status=200)
        return HttpResponse(status=200)

    def get(self, request, *args, **kwargs):
        pass


class SearchView(generic.View):
    template_name = 'orders/results.html'

    def post(self, request, *args, **kwargs):
        context = {}
        query = request.POST.get('query', '')
        if query:
            # Search orders based on order_id or external_id
            orders = Order.objects.filter(Q(event_id__icontains=query) | Q(external_id__icontains=query))

            # Search for shipments based on tracking_code, shipment_id, and sku:
            shipments = Shipment.objects.filter(Q(shipment_id__icontains=query) |
                                                Q(tracking_code__icontains=query) |
                                                Q(shipment_id__icontains=query) |
                                                Q(sku__icontains=query) |
                                                Q(product_name__icontains=query))

            context['orders'] = orders
            context['orders_found'] = len(orders)
            context['shipments'] = shipments
            context['shipments_found'] = len(shipments)
            return render_to_response(self.template_name, context, context_instance=RequestContext(request))


class CreateShippingLabelsView(generic.View):
    template_name = 'orders/easypost_labels.html'

    def get(self, request, *args, **kwargs):
        context = {}
        context['event_ids'] = set(Order.objects.all().values_list('event_id', flat=True))
        return render_to_response(self.template_name, context, context_instance=RequestContext(request))

    def post(self, request, *args, **kwargs):
        pass


class EasypostShippingLabels(generic.View):
    template_name = "orders/batch_list.html"

    def get(self, request, *args, **kwargs):
        eid = request.GET.get('eid', '')
        # Get one random order
        one_order = Order.objects.filter(event_id=eid).first()

        # get customer now
        customer = Customer.objects.get(name=one_order.customer)

        # order file settings
        of_settings = customer.orderfilesettings_set.all().first()
        labels = ShippingLabels.objects.filter(order_file_id=of_settings.id)

        context = {
            'labels': labels
        }

        return render_to_response(self.template_name, context, context_instance=RequestContext(request))


class EnterOrderManually(generic.View):
    form = CreateOrderForm
    template_name = "orders/enter_order_manually.html"

    def get(self, request, *args, **kwargs):
        context = {}
        context['form'] = self.form()
        return render_to_response(self.template_name,
                                  context,
                                  context_instance=RequestContext(request))

    def post(self, request, *args, **kwargs):
        order_form = self.form(request.POST)
        context = {}
        if order_form.is_valid():
            order_instance, _ = order_form.get_order()
            shipping_address, _ = order_form.get_shpping_address()
            shipment, _ = order_form.get_shipment(order_instance,
                                                  shipping_address)
            return HttpResponseRedirect(reverse_lazy('orders:shipments_list'))
        else:
            context['form'] = order_form
            return render_to_response(self.template_name,
                                      context,
                                      context_instance=RequestContext(request))


class EditOrderManually(generic.View):
    template_name = "orders/enter_order_manually.html"

    def get(self, request, *args, **kwargs):
        context = {}

        oid = request.GET.get('oid')
        order = Order.objects.get(id=oid)
        context['form'] = UpdateOrderForm(initial_order=order)
        context['oid'] = oid
        return render_to_response(self.template_name,
                                  context,
                                  context_instance=RequestContext(request))

    def post(self, request, *args, **kwargs):
        context = {}
        oid = request.GET.get('oid')
        initial_order = Order.objects.get(id=oid)
        update_form = UpdateOrderForm(request.POST)
        if update_form.is_valid():
            # Update all instances.
            update_form.process_data(initial_order=initial_order)
            return HttpResponseRedirect(reverse_lazy('orders:shipments_list'))

        context['form'] = update_form
        return render_to_response(self.template_name,
                                  context,
                                  context_instance=RequestContext(request))


class RebuyShipments(generic.View):
    def get(self, request, *args, **kwargs):
        eid = request.GET.get('eid')
        if eid:
            failed_shipments = Shipment.objects.filter(order__event_id=eid,
                                                       purchased=False)
            if failed_shipments:
                customer = failed_shipments[0].order.customer
                customer_settings = Customer.objects.get(name=customer)
                info_dicts = []
                for failed_shipment in failed_shipments:
                    info_dicts.append(
                        {
                            'order': failed_shipment.order,
                            'customer_settings': customer_settings
                        }
                    )

                async_orders_to_easypost(info_dicts)

                # Ugly hack :(
                url = reverse('orders:shipments_list')
                return HttpResponseRedirect(url+'?eid={0}'.format(eid))
