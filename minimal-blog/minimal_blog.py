"""
A minimal set of views for a client which wanted to add a blog section
to his existing django project.
"""

from django.core.urlresolvers import reverse
from django.db.models import Q
from django.views.generic import ListView, DetailView
from django.views.generic.detail import SingleObjectMixin

from blog.models import Article, Tag, Category


class SideNavigation(object):

    def get_context_data(self, **kwargs):
        context = super(SideNavigation, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        context['tags'] = Tag.objects.all()
        return context


class ArticleListView(SideNavigation, ListView):

    template_name = "blog/articles_list.html"
    model = Article

    def get_queryset(self):
        queryset = super(ArticleListView, self).get_queryset()
        author = self.request.GET.get('author')
        category = self.request.GET.get('category')
        tag = self.request.GET.get('tag')

        q = Q(author__id=author) if author else Q()
        q &= Q(tags__name__icontains=tag) if tag else Q()
        q &= Q(category__name__icontains=category) if category else Q()
        return queryset.filter(q, is_published=True).distinct()


class ArticleDetailView(SideNavigation, DetailView, SingleObjectMixin):

    template_name = "blog/article_detail.html"
    model = Article

    def get_success_url(self):
        return reverse('article-detail',
                       kwargs={'slug': self.get_object().slug})

    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        context = super(ArticleDetailView, self).get_context_data(**kwargs)
        return context
