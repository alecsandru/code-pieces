"""
Below is a piece of code from a module which was designed to ingest
various .xlsx order files which will be later translated into objects in
the database. The tricky part here was that I had to find a way for the
customer to configure the way _process_order() method was ingesting the data.

This translated into a Django model which stored a mapping between the 
fields necesary for our backend and their coresspondent in the .xlsx files.

In this way, you could ingest whatever .xlsx files as long as all the information
needed in the backend could be found and mapped in django admin.

This method is a little bit slow, but it offers a lot of flexibility.
"""



from datetime import datetime
from openpyxl import load_workbook
from orders.models import Order, Shipment, ShippingAddress, SaleHistory


def _digest_xlsx(file):
    """Based on an input xlsx file, build a dictionary with orders."""
    active_worksheet = load_workbook(file).worksheets[0]
    # Here we assume that first row will always be the row with titles.
    row_with_title = [cell.value for cell in active_worksheet.rows[0]]
    orders = []
    for order in active_worksheet.rows[1:]:
        order_info = {}
        for i in range(len(row_with_title)):
            order_info[row_with_title[i]] = order[i].value
        orders.append(order_info)
    return orders


def _digest_csv(file):
    pass


def _process_order(order, file_settings, additional_info=None, order_file_instance=None):
    """Receives a dictionary with order informations,
    and a set of customer_settings based on which will
    create:
    orders.Order object
    orders.Shipment object
    orders.ShipmentAddress object
    orders.SaleHistory object"""

    customer = file_settings.customer

    #XXX: Create order:
    order_initial_dict = file_settings.get_order_fields_dict()
    order_instance, created = Order.objects.get_or_create(
        external_id=order.get(order_initial_dict['external_id']),
        event_id=order_file_instance.event_id,
        po_number=order_file_instance.po_number,
        created_at=datetime.now(),
        #Change this
        ship_by_date=datetime.now(),
        customer=file_settings.customer.name,
        files=order_file_instance

    )

    #XXX: Create ShippingAddress instance
    shipping_instance_dict = file_settings.get_shipping_fields_dict()
    shipping_instance, created = ShippingAddress.objects.get_or_create(
        billing_first_name=order.get(shipping_instance_dict['billing_first_name']),
        billing_last_name=order.get(shipping_instance_dict['billing_last_name']),
        shipping_first_name=order.get(shipping_instance_dict['shipping_first_name']),
        shipping_last_name=order.get(shipping_instance_dict['shipping_last_name']),
        shipping_company=order.get(shipping_instance_dict['shipping_company']),
        shipping_address_1=order.get(shipping_instance_dict['shipping_address_1']),
        shipping_address_2=order.get(shipping_instance_dict['shipping_address_2']),
        shipping_address_3=order.get(shipping_instance_dict['shipping_address_3']),
        shipping_city=order.get(shipping_instance_dict['shipping_city']),
        shipping_region=order.get(shipping_instance_dict['shipping_region']),
        shipping_postal_code=order.get(shipping_instance_dict['shipping_postal_code']),
        shipping_phone=order.get(shipping_instance_dict['shipping_phone']),
    )

    if shipping_instance.shipping_phone == '' or shipping_instance.shipping_phone is None or\
                    shipping_instance.shipping_phone == 'unknown':
        shipping_instance.shipping_phone = customer.fallback_phone_number
        shipping_instance.save()


    # Create Shippment instance:
    shippment_initial_dict = file_settings.get_shippment_fields_dict()
    shippment_instance, created = Shipment.objects.get_or_create(
        order=order_instance,
        shipping_address=shipping_instance,
        customer_order_id=order.get(shippment_initial_dict['customer_order_id']),
        customer_product_id=order.get(shippment_initial_dict['customer_product_id']),
        sku=order.get(shippment_initial_dict['sku']),
        product_name=order.get(shippment_initial_dict['product_name']),
        size=order.get(shippment_initial_dict['size']),
        color=order.get(shippment_initial_dict['color']),
        quantity=order.get(shippment_initial_dict['quantity'], 1),
        personalization_type=order.get(shippment_initial_dict['personalization_type'], ''),
        personalization_color=order.get(shippment_initial_dict['personalization_color'], ''),
        personalization_text=order.get(shippment_initial_dict['personalization_text'], '')
    )

    sale_history_initial_dict = file_settings.get_sale_history_fields_dict()
    sale_history_instance = SaleHistory.objects.get_or_create(
        sku=order.get(sale_history_initial_dict['sku']),
        quantity=order.get(sale_history_initial_dict['quantity']),
        customer=file_settings.customer.name,
        date=datetime.now(),
        price='1'
    )

    if additional_info:
        invoice_fields = file_settings.get_invoice_fields_dict()
        for info in additional_info:
            ceva = SaleHistory.objects.filter(sku=info[invoice_fields['invoice_product_sku']]).update(price=info['Unit Price'])

    return order_instance


def digest_file(file_1, file_2=None, file_settings=None, order_file_instance=None):
    if file_1.name.endswith('.xlsx'):
        orders = _digest_xlsx(file_1)
        if file_2 and file_2.name.endswith('.xlsx'):
            orders_additinal_info = _digest_xlsx(file_2)
        failed = 0
        passed = 0
        order_instances = []
        # Find a way to speed up this
        for order in orders:
            try:
                if orders_additinal_info:
                    order_instances.append(_process_order(order, file_settings, orders_additinal_info,
                                                          order_file_instance=order_file_instance))
                    passed +=1
                else:
                    order_instances.append(_process_order(order, file_settings))
                    passed += 1
            except Exception as e:
                failed += 1
        return passed, failed, order_instances
    elif file_1.name.endswith('.csv'):
        return _digest_csv(file_1)


def process_order_file(order_file, file_settings):
    if order_file.file_2:
        return digest_file(file_1=order_file.file.file,
                           file_2=order_file.file_2.file,
                           file_settings=file_settings,
                           order_file_instance=order_file)
    else:
        return digest_file(file_1=order_file.file.file,
                           file_settings=file_settings)
