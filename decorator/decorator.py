"""
A small decorator used to wrap methods such as get() in order to offerr
a cleaner error handling solution all acros the application.
"""
def class_view_error_handler(f):
    @wraps(f)
    def wrapper(view, request, *args, **kwargs):
        try:
            response = f(view, request, *args, **kwargs)
        except ApiError as e:
            return render_simple_response(e.error_code, e.message,
                                          status=e.status)
        else:
            return response

    return wrapper
