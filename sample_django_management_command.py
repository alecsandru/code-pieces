"""
Simple Django Management command used to import various pricing models in the system for 3 different user tipes.

Input file is .XLSX and it uses openpyxl to parse it.

A lot of c/p done inside it due to the fact that the project contained multiple terminologies for the same entities
and it required a quick turnaround.
"""

from django.core.management import BaseCommand
from openpyxl import load_workbook

from service_providers.models import Rate

SESSION_TYPES_MAPPING = {

}

CLIENT_FIELD_MAPPING = {

}


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '--file',
            type=str,
            help='Path to pricing xlsx file'
        )

    def _process_sheet(self, excel_sheet, sheet_type):
        FIELD_MAPPING = CLIENT_FIELD_MAPPING

        rate_dict = dict()
        rows = [x for x in excel_sheet.rows]
        for row in rows:
            cell_values = [cell.value for cell in row]
            if (cell_values[0] != None and cell_values[1] == None and cell_values[2] == None) or\
                    (cell_values[0] == None and cell_values[1] == None):
                # This means we're grabbing the type of rate
                if rate_dict:
                    initial_data = Rate.initial_data()
                    rate_type = rate_dict.pop('type')
                    # Delete anything that already exists
                    existing_rate_object = Rate.objects.filter(
                        type=rate_type
                    ).first()
                    if existing_rate_object:
                        Rate.objects.filter(
                            pk=existing_rate_object.pk
                        ).update(
                            **rate_dict
                        )
                        print("Updated {0}".format(rate_type))
                        # Empty the dict
                        rate_dict = dict()
                    else:
                        initial_data['type'] = rate_type

                        rate_object = Rate.objects.create(
                            **initial_data
                        )

                        # Update it with the new values
                        Rate.objects.filter(
                            pk=rate_object.pk
                        ).update(
                            **rate_dict
                        )
                        print('Created {0}'.format(rate_type))
                        # Empty the dict
                        rate_dict = dict()

                rate_type = cell_values[0]
                rate_dict['type'] = SESSION_TYPES_MAPPING.get(rate_type)
            else:
                field = cell_values[0]
                field_value = cell_values[2]
                rate_field = FIELD_MAPPING.get(field)
                if rate_field:
                    if sheet_type == 'providers':
                        rate_field = rate_field + '_provider'
                    if sheet_type == 'additional':
                        rate_field = rate_field + '_add_client_provider'
                    rate_dict[rate_field] = field_value

        if rate_dict:
            initial_data = Rate.initial_data()
            rate_type = rate_dict.pop('type')
            if rate_type:
                # Delete anything that already exists
                existing_rate_object = Rate.objects.filter(
                    type=rate_type
                ).first()
                if existing_rate_object:
                    Rate.objects.filter(
                        pk=existing_rate_object.pk
                    ).update(
                        **rate_dict
                    )
                    print("Updated {0}".format(rate_type))
                    # Empty the dict
                    rate_dict = dict()
                else:
                    initial_data['type'] = rate_type

                    rate_object = Rate.objects.create(
                        **initial_data
                    )

                    # Update it with the new values
                    Rate.objects.filter(
                        pk=rate_object.pk
                    ).update(
                        **rate_dict
                    )
                    print('Created {0}'.format(rate_type))
                    # Empty the dict
                    rate_dict = dict()

    def handle(self, *args, **options):
        file_path = options['file']
        loaded_xlsx = load_workbook(file_path, data_only=True)

        clients_sheet = loaded_xlsx['clients']
        providers_sheet = loaded_xlsx['providers']
        additional_sheet = loaded_xlsx['additional']

        print("Processing pricing for Clients")
        self._process_sheet(clients_sheet, 'clients')

        print("Processing payouts for Providers")
        self._process_sheet(providers_sheet, 'providers')

        print("Processing payouts for Additional")
        self._process_sheet(additional_sheet, 'additional')



